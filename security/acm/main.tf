data "aws_route53_zone" "host_zone" {
  name = var.hosted_zone
}

provider "aws" {
  region = "us-east-1"
  alias  = "region"
}

resource "aws_acm_certificate" "cert" {
  provider          = aws.region
  domain_name       = var.fqdn
  validation_method = "DNS"
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "cert_validation" {
  provider = aws.region
  name     = aws_acm_certificate.cert.domain_validation_options.0.resource_record_name
  type     = aws_acm_certificate.cert.domain_validation_options.0.resource_record_type
  zone_id  = data.aws_route53_zone.host_zone.id
  records  = [aws_acm_certificate.cert.domain_validation_options.0.resource_record_value]
  ttl      = 60
}

resource "aws_acm_certificate_validation" "cert" {
  provider                = aws.region
  certificate_arn         = aws_acm_certificate.cert.arn
  validation_record_fqdns = [aws_route53_record.cert_validation.fqdn]
  depends_on = [
    aws_route53_record.cert_validation
  ]
}