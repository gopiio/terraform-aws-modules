variable "region" {
    description = "ACM certificate region"
}
variable "hosted_zone" {
    description = "Route53 host zone"
}

variable "fqdn" {
    description = "Fully qualified domain name"
}