# ACM Certificate

Create public certificate in Amazon Ceritificate manager

```hcl
module "acm" {
    source = "https://gitlab.com/gopiio/terraform-aws-modules.git//security/acm"
    region = "replace"
    fqdn = "replace"
    hosted_zone = "repalce"
}
```

## Inputs

| Name        | Description            | Type   | Required | Default |
| :---------- | :--------------------- | :----- | :------- | :------ |
| region      | Cert region            | string | Y        | -       |
| fqdn        | Fully qualified domain | string | Y        | -       |
| hosted_zone | Route53 Hosted Zone    | string | Y        | -       |

## Outputs

| Name | Description | Type   |
| :--- | :---------- | :----- |
| arn  | Cert ARN    | string |
