resource "aws_s3_bucket" "this" { 
  bucket = var.name
  acl = var.acl
  tags = var.tags
  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET", "HEAD"]
    allowed_origins = ["*"]
    max_age_seconds = 3000
  }
}
