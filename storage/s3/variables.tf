variable "name" {
    description = "Bucket name"
}

variable "acl" {
    default = "private"
}

variable "tags" {
    default = {
        "managed-by" = "terraform"
    }
}