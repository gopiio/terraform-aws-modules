# Cloudfront S3 Origin

Setup the Cloudfront endpoint with custom domain for S3 Origin

```hcl
module "cf_s3_origin" {
    source = "https://gitlab.com/gopiio/terraform-aws-modules.git//content-delivery/cloudfront/s3origin"
    s3_bucket_domain_name = "replace"
    s3_bucket_arn = "replace"
    certificate_arn = "replace"
    fqdn = "replace"
    hosted_zone = "repalce"
}
```

## Inputs

| Name                  | Description            | Type   | Required | Default |
| :-------------------- | :--------------------- | :----- | :------- | :------ |
| s3_bucket_domain_name | Bucket Domain name     | string | Y        | -       |
| s3_bucket_id          | Bucket name            | string | Y        | -       |
| s3_bucket_arn         | Bucket ARN             | string | Y        | -       |
| min_ttl               | CF Min ttl             | number | N        | 0       |
| default_ttl           | CF Default TTL         | number | N        | 300     |
| max_ttl               | CF Max ttl             | number | N        | 86400   |
| certificate_arn       | ACM Certificate ARN    | string | Y        | -       |
| fqdn                  | Fully qualified domain | string | Y        | -       |
| hosted_zone           | Route53 Hosted Zone    | string | Y        | -       |


## Outputs

