resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = var.s3_bucket_id
}

locals {
  s3_origin_id = var.s3_bucket_id
}

resource "aws_cloudfront_distribution" "toS3Origin" {
  origin {
    domain_name = var.s3_bucket_domain_name
    origin_id   = local.s3_origin_id

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path
    }
    custom_header {
      name = "Access-Control-Allow-Origin"
      value = "*"
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "Cloudfront disctibution for S3 Origin ${var.s3_bucket_id}"
  default_root_object = var.default_root_object

  aliases = var.aliases
  custom_error_response {
      error_caching_min_ttl = 0
      error_code = 404
      response_code = 200
      response_page_path = var.error_response_page
    }

    custom_error_response {
      error_caching_min_ttl = 0
      error_code = 403
      response_code = 200
      response_page_path = var.error_response_page
    }
  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD","OPTIONS"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.s3_origin_id
    compress         = true

    forwarded_values {
      query_string = false
      headers = [
        "Origin",
        "Access-Control-Request-Headers",
        "Access-Control-Request-Method"
      ]

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = var.min_ttl
    default_ttl            = var.default_ttl
    max_ttl                = var.max_ttl
  }

  viewer_certificate {
    acm_certificate_arn = var.certificate_arn
    ssl_support_method  = "sni-only"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
}
