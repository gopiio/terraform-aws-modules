data "aws_route53_zone" "hosted_zone" {
  name = var.hosted_zone
}

resource "aws_route53_record" "alias_record" {
  zone_id = data.aws_route53_zone.hosted_zone.zone_id
  name    = var.fqdn
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.toS3Origin.domain_name
    zone_id                = aws_cloudfront_distribution.toS3Origin.hosted_zone_id
    evaluate_target_health = false
  }
}