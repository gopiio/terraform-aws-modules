variable "s3_bucket_domain_name" {
  description = "Domain Name of the Origin S3 bucket"
}

variable "s3_bucket_id" {
  description = "Name of the Origin S3 bucket"
}

variable "s3_bucket_arn" {
  description = "ARN of the Origin S3 bucket"
}

variable "min_ttl" {
  default = 0
}

variable "default_ttl" {
  default = 300
}

variable "max_ttl" {
  default = 86400
}

variable "certificate_arn" {
  description = "ARN value of the ACM certificate"
}

variable "fqdn" {
  description = "Alias name for the cloud front distribution"
}

variable "aliases" {
  type        = list(string)
  description = "Aliases"
  default = null
}

variable "hosted_zone" {
  description = "Route53 hosted zone domain name"
}

variable "default_root_object" {
  default = "index.html"
}

variable "error_response_page" {
  default = "/index.html"
}