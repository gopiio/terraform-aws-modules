data "aws_iam_policy_document" "s3Policy" {
  statement {
      actions = ["s3:GetObject"]
      resources = ["${var.s3_bucket_arn}/*"]

      principals {
          type = "AWS"
          identifiers = [aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn] 
      }
  }

  statement {
      actions = ["s3:ListBucket"]
      resources = ["${var.s3_bucket_arn}"]

      principals {
          type = "AWS"
          identifiers = [aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn]
      }
  }
}

resource "aws_s3_bucket_policy" "s3_bucket_policy" {
  bucket = var.s3_bucket_id
  policy = data.aws_iam_policy_document.s3Policy.json
}