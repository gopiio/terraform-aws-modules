output "execution_arn" {
  value = aws_api_gateway_deployment.this.execution_arn
}

output "id" {
  value = aws_api_gateway_deployment.this.id
}

output "invoke_url" {
  value = aws_api_gateway_deployment.this.invoke_url
}

output "creation_date" {
  value = aws_api_gateway_deployment.this.created_date
}