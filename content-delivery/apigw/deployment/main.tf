resource "aws_api_gateway_deployment" "this" {
  rest_api_id = var.rest_api_id
  description = var.description

  stage_name        = var.stage_name
  stage_description = var.stage_description
  variables         = var.stage_name == null ? null : var.stage_vars

  lifecycle {
    create_before_destroy = true
  }

  depends_on = [var.dependencies]
  tags       = var.tags
}