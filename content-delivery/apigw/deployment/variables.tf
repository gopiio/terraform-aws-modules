variable "rest_api_id" {
  description = "The ID of the REST API to deploy"
  type        = string
}

variable "description" {
  description = "Deployment description"
  type        = string
  default     = null
}

variable "stage_name" {
  description = "Name of the stage to deploy"
  type        = string
  default     = null
}

variable "stage_description" {
  description = "Description of the stage"
  type        = string
  default     = null
}

variable "stage_vars" {
  description = "Variables for the stage"
  type        = map(any)
  default     = null
}


variable "dependencies" {
  description = "List of resources that must be created before this one"
  type        = list(object({}))
  default     = null
}
variable "tags" {
  description = "Tags to mark the element with"
  type        = map(string)
  default     = null
}