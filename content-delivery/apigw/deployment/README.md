# API Gateway Deployment

Create an AWS APIGW Deployment resource.

## Input variables

| Name                | Description                     | Type   | Required | Default   |
| ---                 | ---                             | ---    | ---      | ---       |
| `rest_api_id`       | ID of the REST API to deploy    | string | Y        | -         |
| `description`       | Description of the deployment   | string | N        | -         |
| `stage_name`        | Name of the stage to deploy     | list   | N        | None      |
| `stage_description` | Description of the stage        | list   | N        | "EDGE"    |
| `stage_vars`        | Variables for the stage         | string | N        | "HEADER"  |
| `dependencies`      | Elements this module depends on | list   | N        | -         |
| `tags`              | Tags to assign to the resource  | map    | N        | -         |

## Outputs

| Name            | Description                                            |
| ---             | ---                                                    |
| `id`            | Rest API resource ID                                   |
| `invoke_url`    | URL to invoke the API pointing to the associated stage |
| `execution_arn` | ARN to be used in Lambda permissions                   | 
| `creation_date` | Date when this deployment was created                  | 