resource "aws_api_gateway_rest_api" "this" {
  name = var.name
  body = ((var.openapi_spec != null) && fileexists(var.openapi_spec)
    ? templatefile(var.openapi_spec, var.openapi_template_vars)
    : null
  )
  api_key_source           = var.key_source
  binary_media_types       = var.binary_media_types
  description              = var.description
  minimum_compression_size = var.minimum_compression_size
  policy                   = var.policy

  endpoint_configuration {
    types = var.endpoint_configuration_types
  }

  depends_on = [var.dependencies]
  tags       = var.tags
}