output "body" {
  description = "Used as a basis for fingerprinting the stage where this API is deployed"
  sensitive   = true
  value       = aws_api_gateway_rest_api.this.body
}

output "execution_arn" {
  value = aws_api_gateway_rest_api.this.execution_arn
}

output "id" {
  value = aws_api_gateway_rest_api.this.id
}

output "root_resource_id" {
  value = aws_api_gateway_rest_api.this.root_resource_id
}