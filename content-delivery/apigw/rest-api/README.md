# API Gateway REST API

Module to deploy an AWS APIGW resource.

## Input variables

| Name                           | Description                                   | Type   | Required | Default  |
| ---                            | ---                                           | ---    | ---      | ---      |
| `name`                         | API Gateway REST API name                     | string | Y        | -        |
| `description`                  | Description of the API                        | string | N        | -        |
| `binary_media_types`           | Binary media types supported by the REST API  | list   | N        | None     |
| `endpoint_configuration_types` | API Endpoint type                             | list   | N        | EDGE     |
| `key_source`                   | Source of the API key for requests            | string | N        | HEADER   |
| `minimum_compression_size`     | Minimum response size to trigger compression  | int    | N        | Disabled |
| `openapi_spec`                 | Path to OpenAPI spec/template file            | string | N        | -        |
| `openapi_template_vars`        | Variables to replace in the spec template     | map    | N        | -        |
| `policy`                       | Policy to control access to the API Gateway   | string | N        | -        |
| `dependencies`                 | Elements this module depends on               | list   | N        | -        |
| `tags`                         | Tags to assign to the resource                | map    | N        | -        |

## Outputs

| Name               | Description                          |
| ---                | ---                                  |
| `id`               | REST API resource ID                 |
| `body`             | OpenAPI rendered document            |
| `execution_arn`    | ARN to be used in Lambda permissions |
| `root_resource_id` | ID of the API's root resource        |