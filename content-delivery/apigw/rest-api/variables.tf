variable "name" {
  description = "Name of the REST API"
  type        = string
}

variable "binary_media_types" {
  description = "List of binary media types supported by the REST API"
  type        = list(string)
  default     = []
}

variable "description" {
  description = "Description of the REST API"
  type        = string
  default     = null
}

variable "endpoint_configuration_types" {
  description = "Can be EDGE (default), REGIONAL or PRIVATE"
  type        = list(string)
  default     = ["EDGE"]
}

variable "key_source" {
  description = "The source of the API key for requests. Can be HEADER (AWS default) or AUTHORIZER."
  type        = string
  default     = "HEADER"
}

variable "minimum_compression_size" {
  description = "Minimum response size to compress. Must be between -1 (disable) and 10485760."
  type        = number
  default     = -1
}

variable "openapi_spec" {
  description = "Path to the file containing the OpenAPI specification. Can be a template."
  type        = string
  default     = null
}

variable "openapi_template_vars" {
  description = "Values to replace in the OpenAPI specification template"
  type        = map(string)
  default     = {}
}

variable "policy" {
  description = "Policy to control access to the API Gateway"
  type        = string
  default     = null
}


variable "dependencies" {
  description = "List of resources that must be created before this one"
  type        = list(object({}))
  default     = null
}
variable "tags" {
  description = "Tags to mark the element with"
  type        = map(string)
  default     = null
}