variable "name" {
  description = "Name of the stage"
  type        = string
}

variable "description" {
  description = "Description of the stage"
  type        = string
  default     = null
}

variable "rest_api_id" {
  description = "ID of the associated REST API"
  type        = string
}

variable "deployment_id" {
  description = "The ID of the deployment to which the stage points"
  type        = string
}

# TODO: add variables for optional parameters
/*
access_log_settings - (Optional) Enables access logs for the API stage. Detailed below.
cache_cluster_enabled - (Optional) Specifies whether a cache cluster is enabled for the stage
cache_cluster_size - (Optional) The size of the cache cluster for the stage, if enabled. Allowed values include 0.5, 1.6, 6.1, 13.5, 28.4, 58.2, 118 and 237.
client_certificate_id - (Optional) The identifier of a client certificate for the stage.
documentation_version - (Optional) The version of the associated API documentation
variables - (Optional) A map that defines the stage variables
tags - (Optional) A map of tags to assign to the resource.
xray_tracing_enabled - (Optional) Whether active tracing with X-ray is enabled. Defaults to false.
*/


variable "dependencies" {
  description = "List of resources that must be created before this one"
  type        = list(object({}))
  default     = null
}
variable "tags" {
  description = "Tags to mark the element with"
  type        = map(string)
  default     = null
}