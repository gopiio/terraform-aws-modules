# API Gateway Stage

Create an AWS APIGW stage resource.

## Input variables

| Name            | Description                              | Type   | Required | Default  |
| ---             | ---                                      | ---    | ---      | ---      |
| `name`          | Name of the stage                        | string | Y        | -        |
| `description`   | Description of the stage                 | string | N        | -        |
| `rest_api_id`   | ID of the associated REST API            | string | Y        | -        |
| `deployment_id` | ID of the deployment the stage points to | string | Y        | -        |
| `dependencies`  | Elements this module depends on          | list   | N        | -        |
| `tags`          | Tags to assign to the resource           | map    | N        | -        |

## Outputs

| Name            | Description                             |
| ---             | ---                                     |
| `id`            | ID of the stage resource                |
| `invoke_url`    | URL to invoke the API pointing to stage |
| `execution_arn` | ARN to be used in Lambda permissions    |