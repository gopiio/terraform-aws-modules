/*
output "arn" {
  # TF plugin doesn't recognize this output but it's mentioned in the docs
  # TODO: test this output/attr existance (tf validate from a module?)
  value = aws_api_gateway_stage.this.arn
}
*/

output "execution_arn" {
  value = aws_api_gateway_stage.this.execution_arn
}

output "id" {
  value = aws_api_gateway_stage.this.id
}

output "invoke_url" {
  value = aws_api_gateway_stage.this.invoke_url
}