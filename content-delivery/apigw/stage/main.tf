resource "aws_api_gateway_stage" "this" {
  stage_name  = var.name
  description = var.description

  deployment_id = var.deployment_id
  rest_api_id   = var.rest_api_id

  # TODO: add optional parameters

  lifecycle {
    create_before_destroy = true
  }

  depends_on = [var.dependencies]
  tags       = var.tags
}