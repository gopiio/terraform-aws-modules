# API Gatway - Auth Method

Module creates a method with a authorozation.

## Inputs

| Name              | Description              | Type   | Required | Default |
| :---------------- | :----------------------- | :----- | :------- | :------ |
| rest_api_id       | API Gateway rest api ID  | string | Y        | -       |
| resource_id       | API Resource ID          | string | Y        | -       |
| method            | Resource HTTP method     | string | Y        | -       |
| authorization     | Authorization method     | string | Y        | -       |
| lambda_invoke_arn | Lambda funtion to invoke | string | Y        | -       |
| authorization_id  | Authorization ID         | string | Y        | -       |

## Outputs

| Name        | Description | Type   |
| :---------- | :---------- | :----- |
| rest_api_id | Rest API ID | string |
