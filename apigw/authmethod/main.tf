resource "aws_api_gateway_method" "method" {
  rest_api_id   = "${var.rest_api_id}"
  resource_id   = "${var.resource_id}"
  http_method   = "${var.method}"
  authorization = "${var.authorization}"
  authorizer_id = "${var.authorization_id}"
}

resource "aws_api_gateway_integration" "integration" {
  rest_api_id             = "${var.rest_api_id}"
  resource_id             = "${var.resource_id}"
  http_method             = "${var.method}"
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "${var.lambda_invoke_arn}"
  depends_on              = ["aws_api_gateway_method.method"]
}

resource "aws_api_gateway_integration_response" "response" {
  rest_api_id = "${var.rest_api_id}"
  resource_id = "${var.resource_id}"
  http_method = "${var.method}"
  status_code = "200"

  response_templates = {
    "application/json" = ""
  }

  depends_on = ["aws_api_gateway_integration.integration"]
}

resource "aws_api_gateway_method_response" "response" {
  rest_api_id = "${var.rest_api_id}"
  resource_id = "${var.resource_id}"
  http_method = "${var.method}"
  status_code = "200"

  response_models = {
    "application/json" = "Empty"
  }

  depends_on = ["aws_api_gateway_method.method"]
}
