# API Gatway - Auth Method

Module enable CORS on a specific resource.

## Inputs

| Name        | Description             | Type   | Required | Default |
| :---------- | :---------------------- | :----- | :------- | :------ |
| rest_api_id | API Gateway rest api ID | string | Y        | -       |
| resource_id | API Resource ID         | string | Y        | -       |

## Outputs

| Name        | Description | Type   |
| :---------- | :---------- | :----- |
| rest_api_id | Rest API ID | string |
