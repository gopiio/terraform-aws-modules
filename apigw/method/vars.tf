variable "rest_api_id" {}

variable "resource_id" {}
variable "method" {}

variable "authorization" {
  default = "NONE"
}

variable "lambda_invoke_arn" {}
